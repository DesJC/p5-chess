/**
 * Gets an array of all legal moves and their outcome
 * 
 * @param {Game} game 
 * @param {boolean} safe (considering the "dont put yourself in check" rule)
 * 
 * @return {object[]}
 */
function getMoves(game, safe = true) {
  let { pieces, turn } = game;
  let possible = [];

  getPiecesCurrentTurn(game).forEach(piece => {
    switch (piece.type) {
      case Type.BISHOP:
        possible.push(...getBishopMoves(piece, pieces));
        break;
      case Type.ROOK:
        possible.push(...getRookMoves(piece, pieces));
        break;
      case Type.QUEEN:
        possible.push(...getQueenMoves(piece, pieces));
        break;
      case Type.KNIGHT:
        possible.push(...getKnightMoves(piece, pieces));
        break;
      case Type.KING:
        possible.push(...getKingMoves(piece, pieces));
        break;
      case Type.PAWN:
        possible.push(...getPawnMoves(piece, game));
    }
  });

  /* avoids infinite recursion, cannot capture with castling anyways*/
  if (safe)
    possible.push(...getCastlingMoves(game));

  /* attach outcomes to the legal moves */
  possible = possible.map(move => {
    return { move, outcome: game.play(move) };
  });

  if (safe)
    possible = possible
      .filter(possible => !isColorInCheck(turn, possible.outcome));

  return possible;
}

/**
 * 
 * @param {Piece} piece 
 * @param {Piece[]} pieces 
 */
function getBishopMoves(piece, pieces) {
  return getCompositeMoves(piece, pieces,
    [Direction.UP_RIGHT, Direction.UP_LEFT, Direction.DOWN_RIGHT, Direction.DOWN_LEFT]);
}

/**
 * 
 * @param {Piece} piece 
 * @param {Piece[]} pieces 
 */
function getRookMoves(piece, pieces) {
  return getCompositeMoves(piece, pieces,
    [Direction.UP, Direction.DOWN, Direction.RIGHT, Direction.LEFT]);
}

/**
 * 
 * @param {Piece} piece 
 * @param {Piece[]} pieces 
 */
function getQueenMoves(piece, pieces) {
  return getCompositeMoves(piece, pieces,
    [Direction.UP, Direction.DOWN, Direction.RIGHT, Direction.LEFT,
    Direction.UP_RIGHT, Direction.UP_LEFT, Direction.DOWN_RIGHT, Direction.DOWN_LEFT]);
}

/**
 * 
 * @param {Piece} piece 
 * @param {Piece[]} pieces 
 */
function getKnightMoves(piece, pieces) {
  return getIndividualMoves(piece, pieces,
    [Direction.L_UP_RIGHT, Direction.L_UP_LEFT, Direction.L_RIGHT_UP, Direction.L_LEFT_UP,
    Direction.L_DOWN_RIGHT, Direction.L_DOWN_LEFT, Direction.L_RIGHT_DOWN, Direction.L_LEFT_DOWN]);
}

/**
 * 
 * @param {Piece} piece 
 * @param {Piece[]} pieces 
 */
function getKingMoves(piece, pieces) {
  return getIndividualMoves(piece, pieces,
    [Direction.UP, Direction.DOWN, Direction.RIGHT, Direction.LEFT,
    Direction.UP_RIGHT, Direction.UP_LEFT, Direction.DOWN_RIGHT, Direction.DOWN_LEFT]);
}

/**
 * 
 * @param {Piece} piece 
 * @param {Game} game 
 */
function getPawnMoves(piece, game) {
  let { pieces, lastMove } = game;
  let possible = [];

  /* capturing in diagonal */
  (piece.color ? [Direction.UP_RIGHT, Direction.UP_LEFT] :
    [Direction.DOWN_RIGHT, Direction.DOWN_LEFT]).forEach(direction => {
      let destination = piece.position.increment(direction);
      if (destination) {
        let pieceAtDestination = getPieceAt(destination, pieces);

        if ((pieceAtDestination && pieceAtDestination.color !== piece.color))
          possible.push(new Move({
            pieceMovement: new PieceMovement(piece, destination),
            capturedPiece: pieceAtDestination
          }));
      }
    });

  let direction = piece.color ? Direction.UP : Direction.DOWN;
  let destination = piece.position.increment(direction);
  /* going forward */
  if (destination && !isPieceAt(destination, pieces)) {
    /* promotion */
    if (destination.y === 1 || destination.y === 8) {
      [Type.ROOK, Type.BISHOP, Type.KNIGHT, Type.QUEEN].forEach(promotableType =>
        possible.push(new Move({
          capturedPiece: piece,
          promotedPiece: new Piece({
            position: destination,
            color: piece.color,
            type: promotableType,
            hasMoved: true
          })
        }))
      );
    }
    else {
      /* regular moving forward by 1 */
      possible.push(new Move({
        pieceMovement: new PieceMovement(piece, destination)
      }));

      if (!piece.hasMoved) {
        destination = destination.increment(direction);
        /* first moving forward by 2 */
        if (!isPieceAt(destination, pieces))
          possible.push(new Move({
            pieceMovement: new PieceMovement(piece, destination)
          }));
      }
    }
  }
  /* en passant */
  if (lastMove && lastMove.pieceMovement) {
    /* piece after it has moved */
    let lastPieceMoved = getPieceAt(lastMove.pieceMovement.destination, pieces);
    let lastPieceDestination = lastPieceMoved.position;
    /* piece's position before it has moved */
    let lastPiecePosition = lastMove.pieceMovement.piece.position;
    if (lastPieceMoved.type === Type.PAWN &&
      Math.abs(lastPiecePosition.y - lastPieceMoved.position.y) === 2 &&
      Math.abs(lastPieceDestination.x - piece.position.x) === 1 &&
      lastPieceDestination.y === piece.position.y) {
      let destination = new Position(
        lastPieceDestination.x,
        lastPieceDestination.y + (piece.color ? 1 : -1)
      );
      possible.push(new Move({
        pieceMovement: new PieceMovement(piece, destination),
        capturedPiece: lastPieceMoved
      }));
    }
  }

  return possible;
}

/**
 * 
 * @param {Game} game 
 */
function getCastlingMoves(game) {
  let possible = [];

  let currentPieces = getPiecesCurrentTurn(game);
  let king = currentPieces.find(isType(Type.KING));
  let rooks = currentPieces.filter(isType(Type.ROOK));

  if (!king.hasMoved) {
    rooks.forEach(rook => {
      if (!rook.hasMoved) {
        let squaresInBetweenCorrect = true;
        let start = rook.position.x > king.position.x ? king.position.x : rook.position.x;
        let end = rook.position.x < king.position.x ? king.position.x : rook.position.x;

        let isKingSide = rook.position.x > king.position.x;

        for (let i = isKingSide ? king.position : rook.position;
          i && i.x <= end && squaresInBetweenCorrect;
          i = i.increment(Direction.RIGHT)) {
          /* squares in-between must be empty */
          if (i.x > start && i.x < end)
            squaresInBetweenCorrect = !isPieceAt(i, game.pieces);
          /* king cannot move out of, through or into check */
          if (squaresInBetweenCorrect && (isKingSide ? i.x - start : end - i.x) <= 2)
            squaresInBetweenCorrect = !isPositionInCheckByColor(i, !game.turn, game);
        }

        if (squaresInBetweenCorrect) {
          let kingDestination = new Position(king.position.x + (isKingSide ? 2 : -2), king.position.y);
          let rookDestination = new Position(rook.position.x + (isKingSide ? -2 : 3), rook.position.y);
          possible.push(new Move({
            pieceMovement: new PieceMovement(king, kingDestination),
            secondaryPieceMovement: new PieceMovement(rook, rookDestination)
          }));
        }
      }
    });
  }

  return possible;
}

/**
 * Gets possible composite moves for a piece
 * i.e. moves that go in a direction one or many times.
 * 
 * @param {Piece} piece
 * @param {Piece[]} pieces
 * @param {Symbol[]} directions
 * 
 * @return {Move[]} possible moves
 */
function getCompositeMoves(piece, pieces, directions) {
  let possible = [];

  directions.forEach(direction => {
    lastPositionIsEmpty = true;
    for (
      i = piece.position.increment(direction);
      i && lastPositionIsEmpty;
      i = i.increment(direction)) {

      let pieceAtDestination = getPieceAt(i, pieces);
      lastPositionIsEmpty = pieceAtDestination === undefined;

      // piece of opposite color or none
      if ((!lastPositionIsEmpty && pieceAtDestination.color !== piece.color)
        || lastPositionIsEmpty)
        possible.push(new Move({
          pieceMovement: new PieceMovement(piece, i),
          capturedPiece: pieceAtDestination
        }));
    }
  });

  return possible;
}

/**
 * Gets possible individual moves for a piece
 * i.e. moves that go in a direction a single time.
 * 
 * @param {object} param
 * @param {Piece} param.piece
 * @param {Piece[]} param.pieces
 * @param {Symbol[]} param.directions
 * 
 * @return {Move[]} possible moves
 */
function getIndividualMoves(piece, pieces, directions) {
  let possible = [];

  directions.forEach(direction => {
    let nextPosition = piece.position.increment(direction);
    if (nextPosition !== undefined) {
      let pieceAtDestination = getPieceAt(nextPosition, pieces);

      // piece of opposite color or none
      if ((pieceAtDestination !== undefined && pieceAtDestination.color !== piece.color)
        || pieceAtDestination === undefined)
        possible.push(new Move({
          pieceMovement: new PieceMovement(piece, nextPosition),
          capturedPiece: pieceAtDestination
        }));
    }
  });

  return possible;
}