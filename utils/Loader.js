/**
 * Loads all Images. Should be called once at start
 * 
 * @returns {object} the images
 */
function loadAllImages() {
  const BASE_PATH = "assets/";
  const ASSETS = {};

  ASSETS.PIECES = {};
  ([true, false]).forEach(color => {
    let colorName = color ? "White" : "Black";

    ASSETS.PIECES[color] = {};
    
    Object.keys(Type).forEach(typeKey => {
      let typeName = "";

      switch (Type[typeKey]) {
        case Type.BISHOP:
          typeName = "Bishop";
          break;
        case Type.KING:
          typeName = "King";
          break;
        case Type.KNIGHT:
          typeName = "Knight";
          break;
        case Type.PAWN:
          typeName = "Pawn";
          break;
        case Type.QUEEN:
          typeName = "Queen";
          break;
        case Type.ROOK:
          typeName = "Rook";
      }

      ASSETS.PIECES[color][Type[typeKey]] =
        loadImage(`${BASE_PATH}${colorName}${typeName}.png`);
    });
  });

  return ASSETS
}

/**
 * Loads all Colors. Should be called once at start
 * 
 * @returns {object} the colors
 */
function loadAllColors() {
  return {
    background: "#E5E5E5",

    checkerBorder: "#765B0C",
    checkerLightSquare: "#EEEED2",
    checkerDarkSquare: "#769656",
    checkerFocusedSquare: "#FFAA1D",

    invalidMoveText: "#BB1111",
    winText: "#00FF00",
    loseText: "#000000",
    drawText: "#0000FF",

    gameOverText: "#0000FF"
  };
}

/**
 * Loads all Text. Should be called once at start
 * 
 * @returns {object} the texts
 */
function loadAllText() {
  let TEXT = {};

  TEXT.INVALID_MOVE = [
    "Illegal Move"
  ];

  TEXT.WIN = [
    "You win"
  ];
  TEXT.LOSE = [
    "You lost"
  ];
  TEXT.DRAW = [
    "Draw"
  ];

  TEXT.GAME_OVER = [
    "Press F5 to start another game"
  ];

  return TEXT;
}