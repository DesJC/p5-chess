/**
 * Builds a move from parsed user input.
 * i.e. given a piece and a square, guesses what the player means to play
 * 
 * @param {Piece} movedPiece 
 * @param {Position} destination 
 * 
 * @returns {Move} the move guessed
 */
function buildMove(movedPiece, destination) {
  let capturedPiece = getPieceAt(destination, GLOBAL.GAME.pieces);
  let promotedPiece = undefined;
  let pieceMovement = new PieceMovement(movedPiece, destination);
  let secondaryPieceMovement = undefined;


  if (movedPiece.type === Type.PAWN) {
    /* If pawn arrives at the end of the board, auto promotion to queen */
    if (destination.y === 8 || destination.y === 1) {
      pieceMovement = undefined;
      capturedPiece = movedPiece;
      promotedPiece = new Piece({
        position: destination,
        color: movedPiece.color,
        type: Type.QUEEN,
        hasMoved: true
      });
    }
    /* en passant */
    else if (!capturedPiece && destination.x !== movedPiece.position.x) {
      capturedPiece = getPieceAt({
        x: destination.x,
        y: destination.y + (movedPiece.color ? -1 : 1)
      }, GLOBAL.GAME.pieces);
    }
  }

  /* castling is handled by Move.equals only comparing pieceMovement
  and not secondaryPieceMovement*/

  return new Move({ capturedPiece, promotedPiece, pieceMovement, secondaryPieceMovement });
}