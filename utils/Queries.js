/**
 * Gets a type predicate for pieces 
 * @param {Symbol} type 
 */
function isType(type) {
  return piece => piece.type === type;
}

/**
 * Gets a color predicate for pieces 
 * @param {boolean} color 
 */
function isColor(color) {
  return piece => piece.color === color;
}

/**
 * Gets a position predicate for pieces 
 * @param {Position} position 
 */
function isAtPosition(position) {
  return piece => Position.equals(position, piece.position);
}

function isKingOfColor(color) {
  return piece => piece.type == Type.KING && piece.color === color;
}

function isDarkSquare(position) {
  return (position.x + position.y) % 2 === 0;
}

/**
 * Gets all pieces of a color from a pieces array
 * @param {boolean} color 
 * @param {Piece[]} pieces 
 */
function getPiecesOfColor(color, pieces) {
  return pieces.filter(isColor(color));
}

/**
 * Gets all pieces of current turn's color
 * @param {Game} game 
 */
function getPiecesCurrentTurn(game) {
  return getPiecesOfColor(game.turn, game.pieces);
}

/**
 * Gets all pieces of a type from a pieces array
 * @param {Symbol} type 
 * @param {Piece[]} pieces 
 */
function getPiecesOfType(type, pieces) {
  return pieces.filter(isType(type));
}

/**
 * Gets the first piece at a position from a pieces array
 * @param {Position} position 
 * @param {Piece[]} pieces 
 */
function getPieceAt(position, pieces) {
  return pieces.find(isAtPosition(position));
}

/**
 * Checks if there is a piece at a position from a pieces array
 * @param {Position} position 
 * @param {Piece[]} pieces 
 */
function isPieceAt(position, pieces) {
  return pieces.some(isAtPosition(position));
}

/**
 * Checks if a position is in check by a color.
 * i.e. if color is white, then can white attack a given position?
 * 
 * @param {Position} position 
 * @param {boolean} color 
 * @param {Game} game 
 */
function isPositionInCheckByColor(position, color, game) {
  if (game.turn !== color)
    game = game.switchTurn();

  let possible = getMoves(game, false);
  return possible
    .filter(possible => possible.move.pieceMovement)
    .map(possible => possible.move.pieceMovement.destination)
    .some(position.equals());
}

/**
 * Checks if a color is in check by the other's
 * 
 * @param {boolean} color 
 * @param {Game} game 
 */
function isColorInCheck(color, game) {
  return game.pieces
    .filter(isKingOfColor(color))
    .map(king => king.position)
    .some(position => isPositionInCheckByColor(position, !color, game));
}

/**
 * 
 * @param {Game} game The current game state
 * @param {Move[]} nextMoves next legal moves
 * 
 * @returns {Symbol|undefined} the game result or undefined if the game must continue
 */
function checkGameResult(game, nextMoves) {
  let { lastMove, turn, pieces } = game;
  if (nextMoves.length === 0) {
    if (isColorInCheck(turn, game))
      /* checkmate */
      return turn ? GameResult.LOSE : GameResult.WIN;
    else
      /* stalemate */
      return GameResult.DRAW;
  }

  if (GLOBAL.STALING_COUNT === 75) {
    /* 75 move rule */
    return GameResult.DRAW;
  }
  else if (!lastMove.capturedPiece && (!lastMove.pieceMovement ||
    lastMove.pieceMovement && !lastMove.pieceMovement.piece.type === Type.PAWN)) {
    GLOBAL.STALING_COUNT++;
  }
  else {
    GLOBAL.STALING_COUNT = 0;
  }

  let numberOfWhitePieces = getPiecesOfColor(true, pieces).length;
  let numberOfBlackPieces = pieces.length - numberOfWhitePieces;
  let colorWithOnlyKing = numberOfWhitePieces === 1 ? true :
    (numberOfBlackPieces === 1 ? false : undefined);
  let nonKingPieces = pieces.filter(piece => piece.type !== Type.KING);
  if (colorWithOnlyKing === true || colorWithOnlyKing === false) {
    if (nonKingPieces.length === 0 || (nonKingPieces.length === 1 &&
      (nonKingPieces[0].type === Type.BISHOP || nonKingPieces[0].type === Type.KNIGHT))) {
      /* Insufficient material (1 king vs. 1 king + (bishop/knight/nothing)) */
      return GameResult.DRAW;
    }
  }

  /* Insufficient material (1 king + bishop vs. 1 king + bishop (on same color)) */
  if (numberOfWhitePieces === 2 && numberOfBlackPieces === 2 &&
    nonKingPieces.every(isType(TYPE.BISHOP))) {
    /* both on the same color */
    if (!(isDarkSquare(nonKingPieces[0].position) ^ isDarkSquare(nonKingPieces[1].position))) {
      return GameResult.DRAW;
    }
  }
}