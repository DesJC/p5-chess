/**
 * Represents an immutable (x, y) in-game position on the checker.
 */
class Position {
  /**
   * X-coordinate (1-8).
   * 
   * @type {number}
   */
  x;
  /**
   * Y-coordinate (1-8).
   * 
   * @type {number}
   */
  y;

  /**
   * Constructs an in-game Position at given (x,y).
   * 
   * @param {number} [x=1]
   * @param {number} [y=1]
   */
  constructor(x = 1, y = 1) {
    this.x = x;
    this.y = y;

    Object.freeze(this);
  }

  /**
   * Checks equality for (x,y) values.
   * 
   * @param {Position} positionA
   * @param {Position} positionB
   * 
   * @returns {boolean} true iff positions are equal
   */
  static equals(positionA, positionB) {
    if (positionA === positionB)
      return true;
    if (!positionA || !positionB)
      return false;

    return positionA.x === positionB.x &&
      positionA.y === positionB.y;
  }

  /**
   * Checks equality for (x,y) values, with given position.
   * Curied.
   * 
   * @returns {function(Position):boolean}
   * true iff this and given position are equal
   */
  equals() {
    return position => Position.equals(this, position);
  }

  /**
   * Gets a new Position relative to this one.
   * @see {DirectionEnum.js}
   * Only if the new Position is in bounds
   * 
   * @param {object} [param = {}]
   * @param {number} [param.x = 0]
   * @param {number} [param.y = 0]
   * 
   * @returns {Position|undefined} a new position or undefined
   */
  increment({ x = 0, y = 0 } = {}) {
    let newPos = new Position(this.x + x, this.y + y);

    if (Position.isInBound(newPos))
      return newPos;
  }

  /**
   * Checks if a Position's x and y are between 1 and 8
   * 
   * @param {Position} position
   * 
   * @returns {boolean} true iff position is in bounds
   */
  static isInBound(position) {
    return position.x >= 1 && position.x <= 8 &&
      position.y >= 1 && position.y <= 8;
  }
}