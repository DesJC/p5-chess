/**
 * Represents the game board for Chess.
 * 
 * Used to save information on how and where to draw the checker on screen.
 */
class Checker {
  /**
   * Total size (width or height) of the checker.
   * includes the border
   * 
   * @type {number}
   */
  size;
  /**
   * Size (width or height) of a single square on the checker.
   * 
   * @type {number}
   */
  squareSize;
  /**
   * Width of the border around the checker.
   * 
   * @type {number}
   */
  borderWidth;
  /**
   * X-coordinate of the top-left corner of the checker.
   * 
   * @type {number}
   */
  x;
  /**
   * Y-coordinate of the top-left corner of the checker.
   * 
   * @type {number}
   */
  y;
  /**
   * X-coordinate offset used by Piece for correct positioning on the checker.
   * @see {Piece.js}
   * 
   * @type {number}
   */
  pieceOffsetX;
  /**
   * Y-coordinate offset used by Piece for correct positioning on the checker.
   * @see {Piece.js}
   * 
   * @type {number}
   */
  pieceOffsetY;

  /**
   * Constucts a Checker at the default position
   */
  constructor() {
    this.resetPosition();
  }

  /**
   * Resets the checker's position and size, according to new canvas size
   */
  resetPosition() {
    /* rough estimate of checker's size */
    this.size = height * 0.8;
    /* exact sizing of checker's components */
    this.squareSize = Math.floor(this.size / 8);
    this.borderWidth = this.squareSize / 5;
    /* recalculating size in case it did not
    evenly divide in squares and borders */
    this.size = (this.squareSize * 8) + (this.borderWidth * 2);

    this.y = (height - this.size) / 2;
    this.x = (width - this.size) / 3;

    this.pieceOffsetX = this.x + this.borderWidth;
    this.pieceOffsetY = this.y + this.borderWidth;
  }

  /**
   * Draws this checker's border and square tiles
   */
  draw() {
    /* border */
    fill(GLOBAL.COLORS.checkerBorder)
    square(this.x, this.y, this.size);

    /* squares */
    for (let i = 1; i <= 8; i++) {
      for (let j = 1; j <= 8; j++) {
        fill(isDarkSquare({ x: i, y: j }) ?
          GLOBAL.COLORS.checkerDarkSquare :
          GLOBAL.COLORS.checkerLightSquare);

        /* change fill for focused square */
        if (GLOBAL.focusedPiece && GLOBAL.focusedPiece.position.equals()({ x: i, y: j })) {
          fill(GLOBAL.COLORS.checkerFocusedSquare);
        }

        square(
          this.x + this.borderWidth + ((i - 1) * this.squareSize),
          this.y + this.borderWidth + ((8 - j) * this.squareSize),
          this.squareSize
        );
      }
    }
  }

  /**
   * Gets the position (in-game) the user's mouse is in
   * 
   * @returns {Position} mouse's position relative to this checker
   */
  getRelativeMousePosition() {
    let offsetCorrectedX = mouseX - (this.x + this.borderWidth);
    let offsetCorrectedY = mouseY - (this.y + this.borderWidth);

    return new Position(
      1 + Math.floor(offsetCorrectedX / this.squareSize),
      8 - Math.floor(offsetCorrectedY / this.squareSize)
    );
  }
}