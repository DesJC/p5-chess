/**
 * Represents an immutable state of the game in-between turns
 */
class Game {
  /**
   * All pieces on the board
   * 
   * @type {Piece[]}
   */
  pieces;
  /**
   * Stack of moves applied to the initial state
   * to get to this.pieces
   * 
   * @type {Move[]}
   */
  moveHistory;
  /**
   * Color of the player whose turn it is.
   * Matches Piece's color (true is white, false for black).
   * @see {Piece.js}
   * 
   * @type {boolean}
   */
  turn;

  /**
   * Constructs a Game with all given attributes
   * 
   * @param {object} [param]
   * @param {Move[]} param.moveHistory 
   * @param {Piece[]} param.pieces
   * @param {boolean} param.turn
   */
  constructor({ pieces, moveHistory, turn } = { pieces: getInitialPieces(), moveHistory: [], turn: true }) {
    this.pieces = pieces;
    this.moveHistory = moveHistory;
    this.turn = turn === true ? true : false;

    Object.freeze(this);
  }

  /**
   * Gets a copy of this Game, with given move played
   * 
   * @param {Move} move 
   * 
   * @returns {Game} the modified Game
   */
  play(move) {
    let moveHistory = [...this.moveHistory];
    moveHistory.push(move);

    return new Game({
      moveHistory,
      pieces: move.apply(this.pieces),
      turn: !this.turn
    });
  }

  /**
   * Gets a copy of this Game, with only turn switched
   * 
   * @returns {Game} the modified Game
   */
  switchTurn() {
    return new Game({
      ...this,
      turn: !this.turn
    });
  }

  /**
   * Last move played or undefined
   * 
   * @type {Move}
   */
  get lastMove() {
    if (this.moveHistory.length === 0)
      return undefined;

    return this.moveHistory[this.moveHistory.length - 1];
  }
}

/**
 * Gets the initial Pieces for a standard Game
 * 
 * @returns {Piece[]} the pieces
 */
function getInitialPieces() {
  let pieces = [];

  /* pawns */
  for (let i = 1; i <= 8; i++) {
    pieces.push(new Piece({
      position: new Position(i, 2),
      color: true,
      type: Type.PAWN
    }));

    pieces.push(new Piece({
      position: new Position(i, 7),
      color: false,
      type: Type.PAWN
    }));
  }

  /* rooks */
  pieces.push(new Piece({
    position: new Position(8, 8),
    color: false,
    type: Type.ROOK
  }));
  pieces.push(new Piece({
    position: new Position(1, 8),
    color: false,
    type: Type.ROOK
  }));
  pieces.push(new Piece({
    position: new Position(1, 1),
    color: true,
    type: Type.ROOK
  }));
  pieces.push(new Piece({
    position: new Position(8, 1),
    color: true,
    type: Type.ROOK
  }));

  /* knights */
  pieces.push(new Piece({
    position: new Position(2, 8),
    color: false,
    type: Type.KNIGHT
  }));
  pieces.push(new Piece({
    position: new Position(7, 8),
    color: false,
    type: Type.KNIGHT
  }));
  pieces.push(new Piece({
    position: new Position(2, 1),
    color: true,
    type: Type.KNIGHT
  }));
  pieces.push(new Piece({
    position: new Position(7, 1),
    color: true,
    type: Type.KNIGHT
  }));

  /* bishops */
  pieces.push(new Piece({
    position: new Position(3, 8),
    color: false,
    type: Type.BISHOP
  }));
  pieces.push(new Piece({
    position: new Position(6, 8),
    color: false,
    type: Type.BISHOP
  }));
  pieces.push(new Piece({
    position: new Position(3, 1),
    color: true,
    type: Type.BISHOP
  }));
  pieces.push(new Piece({
    position: new Position(6, 1),
    color: true,
    type: Type.BISHOP
  }));

  /* queens */
  pieces.push(new Piece({
    position: new Position(4, 8),
    color: false,
    type: Type.QUEEN
  }));
  pieces.push(new Piece({
    position: new Position(4, 1),
    color: true,
    type: Type.QUEEN
  }));

  /* kings */
  pieces.push(new Piece({
    position: new Position(5, 8),
    color: false,
    type: Type.KING
  }));
  pieces.push(new Piece({
    position: new Position(5, 1),
    color: true,
    type: Type.KING
  }));

  return pieces;
}