/**
 * A single countdown Clock for the chess game.
 * All times are in milliseconds, unless otherwise specified.
 */
class Clock {
  /**
   * Is the clock going
   * 
   * @type {boolean}
   */
  running;
  /**
   * Initial time the clock was constructed with.
   * Used for resets.
   * 
   * @type {number}
   */
  initialTime;
  /**
   * Current time of this Clock
   * 
   * @type {number}
   */
  time;
  /**
   * In-program x-coordinate of the left-center of the clock
   * 
   * @type {number}
   */
  x;
  /**
   * In-program y-coordinate of the left-center of the clock
   * 
   * @type {number}
   */
  y;

  /**
   * Constructs a new Clock with given initialTime and position
   * 
   * @param {number} initialTime 
   * @param {number} x 
   * @param {number} y 
   */
  constructor(initialTime, x, y) {
    this.running = false;
    this.initialTime = initialTime;
    this.time = initialTime;

    this.resetPosition(x, y);
  }

  /**
   * Starts the counting down of this Clock.
   * Will trigger BEFORE_START and AFTER_START.
   */
  start() {
    if (this[ClockEvents.BEFORE_START])
      this[ClockEvents.BEFORE_START](this.time);

    this.running = true;
    this._lastTime = millis();
    this._intervalId = setInterval(() => this._update(), 5);

    if (this[ClockEvents.AFTER_START])
      this[ClockEvents.AFTER_START](this.time);
  }

  /**
   * Stops (still saves current time) the counting down of this Clock.
   * Will trigger BEFORE_STOP and AFTER_STOP.
   */
  stop() {
    if (this[ClockEvents.BEFORE_STOP])
      this[ClockEvents.BEFORE_STOP](this.time);

    this.running = false;
    clearInterval(this._intervalId);

    if (this[ClockEvents.AFTER_STOP])
      this[ClockEvents.AFTER_STOP](this.time);
  }

  /**
   * Resets time to initialTime, also stops the clock if running.
   * Will trigger BEFORE_RESET, BEFORE_STOP, AFTER_STOP, AFTER_RESET
   */
  reset() {
    if (this[ClockEvents.BEFORE_RESET])
      this[ClockEvents.BEFORE_RESET](this.time);

    if (this.running)
      this.stop();
    this.time = this.initialTime;

    if (this[ClockEvents.AFTER_RESET])
      this[ClockEvents.AFTER_RESET](this.time);
  }

  /**
   * Subcribes a handler to an event
   * @see {ClockEvents.js}
   * 
   * @param {Symbol} event 
   * @param {function} handler 
   */
  subscribe(event, handler) {
    this[event] = handler;
  }

  /**
   * Used to update this Clock's current time
   */
  _update() {
    if (!this.running)
      return;

    this.time -= millis() - this._lastTime;
    this._lastTime = millis();

    if (this.time <= 0) {
      this.time = 0;
      this.stop();
    }
  }

  /**
   * Adds time to this Clock
   * 
   * @param {number} time in miiliseconds
   * 
   * @returns {number} time after addition
   */
  add(time) {
    return this.time += time;
  }

  /**
   * Draws this Clock on screen
   */
  draw() {
    fill("#000000");
    textAlign(LEFT, CENTER);
    textSize(this._fontSize);
    text(this.formattedTime, this.x, this.y);
  }

  /**
   * Resets the Clock's position and font size, according to new canvas size
   * 
   * @param {number} x 
   * @param {number} y 
   */
  resetPosition(x, y) {
    this.x = x;
    this.y = y;
    this._fontSize = 70 * (height / 1000);
  }

  /**
   * Gets minutes and seconds in a 00:00 format
   * 
   * @returns {string} formatted time
   */
  get formattedTime() {
    let seconds = `${Math.floor(this.time / 1000) % 60}`;
    seconds = "0".repeat(2 - seconds.length) + seconds;
    let minutes = `${Math.floor(this.time / 60000) % 60}`;
    minutes = "0".repeat(2 - minutes.length) + minutes;

    return `${minutes}:${seconds}`;
  }
}