/**
 * Represents an immutable chess piece
 */
class Piece {
  /**
   * Current position of this Piece
   * @type {Position}
   */
  position;
  /**
   * Color of this Piece.
   * True for white, false for black.
   * 
   * @type {boolean}
   */
  color;
  /**
   * Type (e.g. pawn, rook, etc.) of this Piece
   * @see {typeEnum.js}
   * 
   * @type {Symbol}
   */
  type;
  /**
   * Has this Piece moved in the current game
   * 
   * @type {boolean}
   */
  hasMoved;

  /**
   * Constructs a Piece with given attributes
   * 
   * @param {object} param
   * @param {Position} param.position
   * @param {boolean} param.color
   * @param {Symbol} param.type
   * @param {boolean} param.hasMoved
   */
  constructor({ position, color, type, hasMoved }) {
    this.position = position;
    this.color = color;
    this.type = type;
    /**
     * Has this Piece moved in the current game
     * @type {boolean}
     */
    this.hasMoved = hasMoved === true ? true : false;

    Object.freeze(this);
  }

  /**
   * Get this Piece's position's x-coordinate.
   * 
   * @type {number}
   */
  get x() {
    return this.position.x;
  }

  /**
   * Get this Piece's position's y-coordinate.
   * 
   * @type {number}
   */
  get y() {
    return this.position.y;
  }

  /**
   * Checks equality for all Piece attributes,
   * except for "hasMoved"
   * 
   * @param {Piece} pieceA
   * @param {Piece} pieceB
   * 
   * @returns {boolean} true iff pieces are equal
   */
  static equals(pieceA, pieceB) {
    if (pieceA === pieceB)
      return true;
    if (!pieceA || !pieceB)
      return false;

    return (Position.equals(pieceA.position, pieceB.position)) &&
      (pieceA.color === pieceB.color) &&
      (pieceA.type === pieceB.type);
  }

  /**
   * Checks equality for all Piece attributes,
   * except for "hasMoved", with a given piece.
   * Curied.
   * 
   * @returns {function(Piece):boolean} true iff this and given piece are equal
   */
  equals() {
    return piece => Piece.equals(this, piece);
  }

  /**
   * Creates a safe copy of this Piece at destination.
   * 
   * @param {Position} destination 
   * 
   * @returns {Piece} moved Piece
   */
  move(destination) {
    return new Piece({
      ...this,
      position: destination,
      hasMoved: true
    });
  }

  /**
   * Draws a Piece on screen
   * 
   * @param {Piece} piece
   */
  static draw(piece) {
    let sprite = GLOBAL.ASSETS.PIECES[piece.color][piece.type];

    image(
      sprite,
      ((piece.x - 1) * GLOBAL.CHECKER.squareSize) + GLOBAL.CHECKER.pieceOffsetX,
      ((8 - piece.y) * GLOBAL.CHECKER.squareSize) + GLOBAL.CHECKER.pieceOffsetY,
      GLOBAL.CHECKER.squareSize,
      GLOBAL.CHECKER.squareSize
    );
  }
}