/**
 * A legal set of modifications of the Pieces in one turn.
 * Immutable
 */
class Move {
  /**
   * Constructs a Move with all possible attributes
   * 
   * @param {object} param
   * @param {Piece} param.capturedPiece
   * @param {Piece} param.promotedPiece
   * @param {PieceMovement} param.pieceMovement
   * @param {PieceMovement} param.secondaryPieceMovement 
   */
  constructor({ capturedPiece, promotedPiece, pieceMovement, secondaryPieceMovement }) {
    this.capturedPiece = capturedPiece;
    this.promotedPiece = promotedPiece;
    this.pieceMovement = pieceMovement
    this.secondaryPieceMovement = secondaryPieceMovement;

    Object.freeze(this);
  }

  /**
   * Checks Equality for all attributes of a Move,
   * except for secondaryPieceMovement
   * 
   * @param {Move} moveA
   * @param {Move} moveB
   * 
   * @returns {boolean} true iff Moves are equal
   */
  static equals(moveA, moveB) {
    if (moveA === moveB)
      return true;
    if (!moveA || !moveB)
      return false;

    return Piece.equals(moveA.capturedPiece, moveB.capturedPiece) &&
      Piece.equals(moveA.promotedPiece, moveB.promotedPiece) &&
      PieceMovement.equals(moveA.pieceMovement, moveB.pieceMovement);
  }

  /**
   * Checks Equality for all attributes of a Move,
   * except for secondaryPieceMovement, with a given Move.
   * Curied.
   *
   * @returns {function(Move):boolean} true iff this and given Move are equal
   */
  equals() {
    return move => Move.equals(this, move);
  }

  /**
   * Applies the move on a copy of pieces
   *
   * @param {Piece[]} pieces
   * 
   * @returns {Piece[]} the new state of pieces
   */
  apply(pieces) {
    let { capturedPiece, promotedPiece, pieceMovement, secondaryPieceMovement } = this;
    pieces = [...pieces];

    /* remove capture Piece */
    if (capturedPiece) {
      pieces.splice(pieces.findIndex(capturedPiece.equals()), 1);
    }
    /* add promoted Piece */
    if (promotedPiece) {
      pieces.push(promotedPiece);
    }
    /* move piece */
    if (pieceMovement) {
      pieces.splice(pieces.findIndex(pieceMovement.piece.equals()), 1);
      pieces.push(pieceMovement.moved);
    }
    /* move piece (in case of castling) */
    if (secondaryPieceMovement) {
      pieces.splice(pieces.findIndex(secondaryPieceMovement.piece.equals()), 1);
      pieces.push(secondaryPieceMovement.moved);
    }

    return pieces;
  }
}
