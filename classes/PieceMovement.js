/**
 * Represents an immutable movement of a single Piece
 */
class PieceMovement {
  /**
   * Piece before it has moved
   * 
   * @type {Piece}
   */
  piece;
  /**
   * Where the Piece is moving
   * 
   * @type {Piece}
   */
  destination;

  /**
   * Construct a PieceMovement with a Piece and its destination
   * 
   * @param {Piece} piece 
   * @param {Position} destination 
   */
  constructor(piece, destination) {
    this.piece = piece;
    this.destination = destination;

    Object.freeze(this);
  }

  /**
   * The Piece after it has moved
   * 
   * @type {Piece}
   */
  get moved() {
    return this.piece.move(this.destination);
  }

  /**
   * Checks equality for all PieceMovement attributes.
   * 
   * @param {PieceMovement} pieceMovementA
   * @param {PieceMovement} pieceMovementB
   * 
   * @returns {boolean} true iff pieceMovements are equal
   */
  static equals(pieceMovementA, pieceMovementB) {
    if (pieceMovementA === pieceMovementB)
      return true;
    if (!pieceMovementA || !pieceMovementB)
      return false;

    return Piece.equals(pieceMovementA.piece, pieceMovementB.piece)
      && Position.equals(pieceMovementA.destination, pieceMovementB.destination);
  }

  /**
   * Checks equality for all PieceMovement attributes.
   * Curied.
   * 
   * @returns {function(PieceMovement):boolean}
   * true iff this PieceMovement and pieceMovement are equal 
   */
  equals() {
    return pieceMovement => PieceMovement.equals(this, pieceMovement);
  }
}