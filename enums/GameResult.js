/**
 * Defines the types of game result (win/draw/lose)
 * @readonly
 * @enum {symbol}
 */
const GameResult = {
  WIN: Symbol("Win"),
  DRAW: Symbol("Draw"),
  LOSE: Symbol("Lose"),
};