/**
 * Defines the main directions of Pieces
 * @readonly
*/
const Direction = {
  UP: { x: 0, y: 1 },
  DOWN: { x: 0, y: -1 },
  RIGHT: { x: 1, y: 0 },
  LEFT: { x: -1, y: 0 },
  UP_RIGHT: { x: 1, y: 1 },
  UP_LEFT: { x: -1, y: 1 },
  DOWN_RIGHT: { x: 1, y: -1 },
  DOWN_LEFT: { x: -1, y: -1 },

  L_UP_RIGHT: { x: 1, y: 2 },
  L_UP_LEFT: { x: -1, y: 2 },
  L_RIGHT_UP: { x: 2, y: 1 },
  L_LEFT_UP: { x: -2, y: 1 },
  L_DOWN_RIGHT: { x: 1, y: -2 },
  L_DOWN_LEFT: { x: -1, y: -2 },
  L_RIGHT_DOWN: { x: 2, y: -1 },
  L_LEFT_DOWN: { x: -2, y: -1 },
};