/**
 * Defines the type of a piece
 * @readonly
 * @enum {symbol}
 */
const Type = {
  PAWN: Symbol("Pawn"),
  ROOK: Symbol("Rook"),
  BISHOP: Symbol("Bishop"),
  KNIGHT: Symbol("Knight"),
  KING: Symbol("King"),
  QUEEN: Symbol("Queen")
};