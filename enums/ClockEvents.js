/**
 * Allows to subscribe to various events
 * @see {Clock.js}
 * @readonly
 * @enum {Symbol}
 */
const ClockEvents = {
  BEFORE_START: Symbol("BeforeStart"),
  AFTER_START: Symbol("AfterStart"),
  BEFORE_STOP: Symbol("BeforeStop"),
  AFTER_STOP: Symbol("AfterStop"),
  BEFORE_RESET: Symbol("BeforeReset"),
  AFTER_RESET: Symbol("AfterReset")
}